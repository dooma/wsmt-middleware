import Pyro.core
import sys


class PyroClient:
    def __init__(self, uri):
        Pyro.core.initClient()
        proxy = Pyro.core.getProxyForURI(uri)
        print("Client RMI Pyro3 Python: " + uri)
        print("ping: \t" + proxy.ping())

        id = proxy.insert_book('Abecedar', 2018, 'Educational', 'Editura Corint')
        print("Create Book: \n\tAbecedar\n\t2018\n\tEductional\n\tEditura Corint\n\tResult id: " + str(id))

        print("Delete Book: Id: " + str(id))
        proxy.delete_book(id)

        print("Filter Book: \n" + '\n'.join([str(item[0]) for item in proxy.filter_book('Life')]))

        id_author = proxy.insert_author('John Vancouver')
        print("Create Author:\n\tJohn Vancouver\n\tResult id: " + str(id_author))

        print("Delete Author: " + str(id_author))
        proxy.delete_author(id_author)

        print("Filter Author: \n" + '\n'.join(proxy.filter_author('John')))


def start(uri):
    PyroClient(uri)
