import Pyro.core
import socket
from datetime import datetime
from database import Author, Book, db, BookAuthor


class PyroServer(Pyro.core.ObjBase):
    def __init__(self):
        Pyro.core.ObjBase.__init__(self)

    def ping(self):
        name = socket.gethostname()
        ip = socket.gethostbyname(name)
        return "Python Exec Pyro3" + ": " + name + \
            "(" + ip + ":7766" + "), " + `datetime.now()`

    def insert_book(self, title, year, genre, author_name):
        book = Book.create(title=title, year=2018, genre=genre)
        try:
            author = Author.get(name=author_name)
        except Author.DoesNotExist:
            author = Author.create(name=author_name)
        finally:
            book_author = BookAuthor(book=book, author=author)
        return book.id

    def delete_book(self, id):
        Book.delete_by_id(id)

    def update_book(self, id, title, year, genre):
        book = Book.get(id=id)
        book.title = title
        book.year = year
        book.genre = genre
        book.save()


    def filter_book(self, title):
        return [(book.title, book.year, book.genre) for book in (Book.select().where(Book.title.contains(title)))]

    def insert_author(self, author_name):
        author = Author.create(name=author_name)
        return author.id

    def delete_author(self, id):
        Author.delete_by_id(id)

    def update_author(self, id, author_name):
        author = Author.get(id=id)
        author.name = author_name
        author.save()

    def filter_author(self, author_name):
        return [author.name for author in (Author.select().where(Author.name.contains(author_name)))]


def start():
    db.connect()
    Pyro.core.initServer()
    daemon = Pyro.core.Daemon()
    uri = daemon.connect(PyroServer(), "exec")
    print("Python Exec Pyro3 waiting with name \"exec\" at: " + `uri`)
    daemon.requestLoop()
