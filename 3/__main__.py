from .server import start as start_server
from .client import start as start_client
import sys

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please provide a command: client or server')
        sys.exit(1)
    if sys.argv[1] == 'client':
        if len(sys.argv) > 2:
            start_client(sys.argv[2])
        else:
            start_client("PYRO:exec@localhost:7543")
    if sys.argv[1] == 'server':
        start_server()