import Pyro4
import sys


class PyroClient:
    def __init__(self, uri):
        proxy = Pyro4.Proxy(uri)
        print("Client RMI Pyro4 Python: " + uri)
        print("ping: \t" + proxy.ping())
        print("Filter Book: \n" + '\n'.join([str(item[0]) for item in proxy.filter_book('Life')]))
        print("Filter Author: \n" + '\n'.join(proxy.filter_author('John')))


def start(uri):
    PyroClient(uri)