﻿using System;
using System.Collections;
using Razorvine.Pyro;

namespace csharpclient
{
    class MainClass
    {
        public static void RunClient(String path)
        {
            Console.WriteLine("Client C# Pyro (pyrolite): " + path);

            var uri = path.Split(new string[] { ":","@" }, StringSplitOptions.RemoveEmptyEntries);

            String server = (String)uri[1];
            String host = (String)uri[2];
            int port = int.Parse((String)uri[3]);
            PyroProxy proxy = new PyroProxy(host, port, server);
            Console.WriteLine("ping: \t" + proxy.call("ping"));
            object books = proxy.call("filter_book", "Life");
            IEnumerable enumerable_books = books as IEnumerable;
            foreach (object[] book in enumerable_books)
            {
                Console.WriteLine(book[0]);
            }
            object authors = proxy.call("filter_author", "John");
            IEnumerable enumerable_authors = authors as IEnumerable;
            foreach (String author in enumerable_authors)
            {
                Console.WriteLine(author);
            }
            proxy.close();
        }

        public static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                RunClient(args[0]);
            }
            else
            {
                RunClient("PYRO:exec@localhost:7543");
            }
        }
    }
}
