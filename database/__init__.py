import six

if six.PY2:
    from models import db, Author, Book, BookAuthor
if six.PY3:
    from .models import db, Author, Book, BookAuthor

book_data = [("Do Androids Dream of Electric Sheep?","Philip K. Dick"),
("The Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy, #1)","Douglas Adams"),
("Something Wicked This Way Comes","Ray Bradbury"),
("Pride and Prejudice and Zombies (Pride and Prejudice and Zombies, #1)","Seth Grahame-Smith"),
("I Was Told There'd Be Cake","Sloane Crosley"),
("The Curious Incident of the Dog in the Night-Time","Mark Haddon"),
("To Kill a Mockingbird","Harper Lee"),
("The Hollow Chocolate Bunnies of the Apocalypse","Robert Rankin"),
("Eats, Shoots & Leaves: The Zero Tolerance Approach to Punctuation","Lynne Truss"),
("The Unbearable Lightness of Being","Milan Kundera"),
("Are You There, Vodka? It's Me, Chelsea","Chelsea Handler"),
("A Clockwork Orange","Anthony Burgess"),
("Midnight in the Garden of Good and Evil","John Berendt"),
("The Perks of Being a Wallflower","Stephen Chbosky"),
("The Man Without Qualities","Robert Musil"),
("The Earth, My Butt, and Other Big Round Things (Virginia Shreves #1)","Carolyn Mackler"),
("Cloudy With a Chance of Meatballs","Judi Barrett"),
("Where the Wild Things Are","Maurice Sendak"),
("John Dies at the End (John Dies at the End, #1)","David  Wong"),
("Me Talk Pretty One Day","David Sedaris"),
("Stop Dressing Your Six-Year-Old Like a Skank: A thern Belle's Words of Wisdom","Celia Rivenbark"),
("The Elephant Tree","R.D. Ronald"),
("One Hundred Years of Solitude","Gabriel Garcia Marquez"),
("A Heartbreaking Work of Staggering Genius","Dave Eggers"),
("The Long Dark Tea-Time of the Soul (Dirk Gently, #2)","Douglas Adams"),
("One Flew Over the Cuckoo's Nest","Ken Kesey"),
("How to Lose Friends and Alienate People","Toby Young"),
("Alexander and the Terrible, Horrible, No Good, Very Bad Day","Judith Viorst"),
("An Arsonist's Guide to Writers' Homes in New England","Brock Clarke"),
("Another Bullshit Night in Suck City","Nick Flynn"),
("Requiem for a Dream","Hubert Selby Jr."),
("I Have No Mouth and I Must Scream","Harlan Ellison"),
("So Long, and Thanks for All the Fish (Hitchhiker's Guide to the Galaxy, #4)","Douglas Adams"),
("Quantum Roots II: Worm Holes","Kyle Keyes"),
("The Silence of the Lambs  (Hannibal Lecter, #2)","Thomas  Harris"),
("A Thousand Splendid Suns","Khaled Hosseini"),
("A Wrinkle in Time (Time Quintet, #1)","Madeleine L'Engle"),
("Love Is the Answer, God Is the Cure: A True Storyal and Unconditional Love","Aimee Cabo Nikolov"),
("Extremely Loud and Incredibly Close","Jonathan Safran Foer"),
("Neverwhere (London Below, #1)","Neil Gaiman"),
("The Catcher in the Rye","J.D. Salinger"),
("If on a Winter's Night a Traveler","Italo Calvino"),
("The Zombie Survival Guide: Complete Protection from the Living Dead","Max Brooks"),
("The Devil Wears Prada (The Devil Wears Prada, #1)","Lauren Weisberger"),
("The Grapes of Wrath","John Steinbeck"),
("Their Eyes Were Watching God","Zora Neale Hurston"),
("I Am America","Stephen Colbert"),
("I Still Miss My Man But My Aim Is Getting Better","Sarah Shankman"),
("The Zombie Room","R.D. Ronald"),
("And Then There Were None","Agatha Christie"),
("Go the Fuck to Sleep","Adam Mansbach"),
("When You Are Engulfed in Flames","David Sedaris"),
("The Man Who Mistook His Wife for a Hat and Other Clinical Tales","Oliver Sacks"),
("The Guernsey Literary and Potato Peel Pie Society","Mary Ann Shaffer"),
("For Whom the Bell Tolls","Ernest Hemingway"),
("Fear and Loathing in Las Vegas","Hunter S. Thompson"),
("English as a Second F*cking Language: How to Sweles Taken from Everyday Life","Sterling Johnson"),
("American Psycho","Bret Easton Ellis"),
("Brave New World","Aldous Huxley"),
("A Confederacy of Dunces","John Kennedy Toole"),
("The Gordonston Ladies Dog Walking Club (The Gord Ladies Dog Walking Club #1)","Duncan Whitehead"),
("Trainspotting","Irvine Welsh"),
("Love in the Time of Cholera","Gabriel Garcia Marquez"),
("Factotum","Charles Bukowski"),
("Skagboys","Irvine Welsh"),
("Zen and the Art of Motorcycle Maintenance: An Inquiry Into Values","Robert M. Pirsig"),
("No Country for Old Men","Cormac McCarthy"),
("Lunar Park","Bret Easton Ellis"),
("Invisible Monsters","Chuck Palahniuk"),
("Tequila Makes Her Clothes Fall Off (Country Music Collection, #1)","Cara North"),
("When Will Jesus Bring the Pork Chops?","George Carlin"),
("The Wasp Factory","Iain Banks"),
("The Restaurant at the End of the Universe (Hitchhiker's Guide to the Galaxy, #2)","Douglas Adams"),
("Send More Idiots","Tony Perez-Giese"),
("All Quiet on the Western Front","Erich Maria Remarque"),
("The Sound and the Fury","William Faulkner"),
("Finding Hope in the Darkness of Grief: Spiritual trough Art, Poetry and Prose","Diamante Lavendar"),
("How to Shit in the Woods: An Environmentally Sound Approach to a Lost Art","Kathleen Meyer"),
("The Spy Who Came In from the Cold","John le Carre"),
("As I Lay Dying","William Faulkner"),
("The Moon is a Harsh Mistress","Robert A. Heinlein"),
("The Man Who Was Thursday: A Nightmare","G.K. Chesterton"),
("The Particular Sadness of Lemon Cake","Aimee Bender"),
("The Lone Ranger and Tonto Fistfight in Heaven","Sherman Alexie"),
("I Hope They Serve Beer in Hell (Tucker Max, #1)","Tucker Max"),
("The Electric Kool-Aid Acid Test","Tom Wolfe"),
("Of Mice and Men","John Steinbeck"),
("Tinker, Tailor, Soldier, Spy","John le Carre"),
("The Scopas Factor","Vincent Panettiere"),
("Fuck This Book","Bodhi Oser"),
("What to Say When You Talk to Yourself","Shad Helmstetter"),
("Chasing the Red Queen","Karen Glista"),
("Nostradamus Ate My Hamster","Robert Rankin"),
("Hard-Boiled Wonderland and the End of the World","Haruki Murakami"),
("Well-Behaved Women Seldom Make History","Laurel Thatcher Ulrich"),
("The Importance of Being Earnest","Oscar Wilde"),
("Don't Pee on My Leg and Tell Me It's Raining: Am Family Court Judge Speaks Out","Judy Sheindlin"),
("The Baby Jesus Butt Plug","Carlton Mellick III"),
("She Got Up Off The Couch: And Other Heroic Acts From Mooreland, Indiana","Haven Kimmel"),
("Life, the Universe and Everything (Hitchhiker's Guide to the Galaxy, #3)","Douglas Adams")]

def populate():
    db.connect()
    db.create_tables([Book, Author, BookAuthor])

    from collections import defaultdict
    values = defaultdict(list)
    for title, author_name in book_data:
        values[author_name].append(title)

    for author_name, titles in values.items():
        author = Author.create(name=author_name)
        for title in titles:
            book = Book.create(title=title, year=2018, genre="Fiction")
            book_author = BookAuthor(book=book, author=author)

def destroy():
    db.connect()
    db.drop_tables([Book, Author, BookAuthor])
    