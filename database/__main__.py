import sys

from database import populate, destroy

if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == 'populate':
        populate()
    if len(sys.argv) == 2 and sys.argv[1] == 'destroy':
        destroy()