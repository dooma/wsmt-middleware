from peewee import *
import datetime


db = SqliteDatabase('library.db')

class BaseModel(Model):
    class Meta:
        database = db

class Book(BaseModel):
    title = CharField()
    year = IntegerField()
    genre = CharField()

class Author(BaseModel):
    name = CharField(unique=True)

class BookAuthor(BaseModel):
    book = ForeignKeyField(Book)
    author = ForeignKeyField(Author)