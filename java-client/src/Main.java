import com.sun.jndi.toolkit.url.Uri;
import net.razorvine.pyro.*;
import net.razorvine.pickle.*;

import java.net.URI;
import java.net.URLDecoder;
import java.util.*;

public class Main {

    public Main(String uri) throws Exception {
        System.out.println("Client Java Pyro (pyrolite): " + uri);
        String[] elements = uri.split("[:@]");
        String server = elements[1];
        String host = elements[2];
        Integer port = Integer.parseInt(elements[3]);

        PyroProxy proxy = new PyroProxy(host, port, server);
        System.out.println("ping: \t" + proxy.call("ping"));
        List<Object[]> books = (ArrayList<Object[]>)proxy.call("filter_book", "Life");
        for(Object[] book : books) {
            System.out.println(book[0]);
        }
        List<String> authors = (ArrayList<String>)proxy.call("filter_author", "John");
        for(String author : authors) {
            System.out.println(author);
        }
        proxy.close();
    }

    public static void main(String[] args) {
        try {
            if (args.length > 0) {
                new Main(args[0]);
            } else {
                new Main("PYRO:exec@localhost:7543");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
